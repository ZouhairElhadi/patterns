# Import the required libraries
from tkinter import *

# Create an instance of tkinter frame or window
from caretaker import ABCCaretaker, Caretaker
from originator import ABCOriginator, Originator


class TestMemento:

    def __init__(self, caretaker: ABCCaretaker,
                 originator: ABCOriginator):
        self._caretaker = caretaker
        self._originator = originator
        self.save_files = 0
        self.current_article = 0
        self.text = None
        self.save = None
        self.undo = None
        self.redo = None

    def save_f(self):
        # get text from text area
        text = self.text.get("1.0", "end-1c")
        # Set the value for the current memento
        self._originator.set(text)
        # Add new article to the ArrayList
        self._caretaker.add_memento(self.current_article, self._originator.store_in_memento())
        # saveFiles monitors how many articles are saved
        # currentArticle monitors the current article displayed
        self.save_files += 1
        self.current_article += 1
        # Make undo clickable
        self.undo["state"] = "normal"

    def undo_f(self):
        if self.current_article >= 1:
            # Decrement to the current article displayed
            self.current_article -= 1
            # Get the older article saved and display it in win
            text = self._originator.restore_from_memento(
                self._caretaker.get_memento(self.current_article)
            )
            self.text.delete("1.0", END)
            self.text.insert("1.0", text)
            # Make redo clickable
            self.redo["state"] = "normal"
        else:
            # Decrement to the current article displayed
            self.current_article -= 1
            # clear txt area
            self.text.delete("1.0", END)
            # disabled undo
            self.undo["state"] = "disabled"

    def redo_f(self):
        if (self.save_files - 1) > self.current_article:
            # increment to the current article displayed
            self.current_article += 1
            # Get the older article saved and display it in win
            text = self._originator.restore_from_memento(
                self._caretaker.get_memento(self.current_article)
            )
            self.text.delete("1.0", END)
            self.text.insert("1.0", text)
            # Make undo clickable
            self.undo["state"] = "normal"
        else:
            # disabled redo
            self.redo["state"] = "disabled"

    def create_win(self):
        win = Tk()
        # Set the size of the window
        win['padx'] = 20
        win['pady'] = 10
        # set a title
        win.title("Memento Design Pattern")
        # Create a Text widget with undo is set
        self.text = Text(win, width=60, height=40)
        self.text.pack(pady=5)
        self.save = Button(win, text="save",
                           command=lambda: self.save_f())
        self.save.pack(pady=5)
        self.redo = Button(win, text="redo",
                           command=lambda: self.redo_f())
        self.redo.pack(pady=5)
        self.redo["state"] = "disabled"
        self.undo = Button(win, text="undo",
                           command=lambda: self.undo_f())
        self.undo.pack(pady=5)
        self.undo["state"] = "disabled"
        win.mainloop()


if __name__ == "__main__":
    test_memento = TestMemento(Caretaker(), Originator())
    test_memento.create_win()
