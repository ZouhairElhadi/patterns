from abc import ABC, abstractmethod

from memento import ABCMemento


class ABCCaretaker(ABC):

    @abstractmethod
    def add_memento(self, index, memento: ABCMemento) -> None:
        pass

    @abstractmethod
    def get_memento(self, index) -> ABCMemento:
        pass


class Caretaker(ABCCaretaker):

    def __init__(self) -> None:
        self._states = list()

    def add_memento(self, index, memento: ABCMemento) -> None:
        if len(self._states) == index:
            self._states.append(memento)
        else:
            self._states[index] = memento

    def get_memento(self, index) -> ABCMemento:
        return self._states[index]
