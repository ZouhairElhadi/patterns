from abc import ABC, abstractmethod

from memento import ABCMemento, Memento


class ABCOriginator(ABC):

    @abstractmethod
    def set(self, state) -> None:
        pass

    @abstractmethod
    def store_in_memento(self) -> ABCMemento:
        pass

    @abstractmethod
    def restore_from_memento(self, memento: ABCMemento) -> str:
        pass


class Originator(ABCOriginator):
    """
    For the sake of simplicity, the originator's state is stored inside a single
    variable.
    """

    def __init__(self) -> None:
        self._state = None

    def set(self, state) -> None:
        """
        The Originator's business logic may affect its internal state.
        Therefore, the client should backup the state before launching methods
        of the business logic via the save() method.
        """

        self._state = state

    def store_in_memento(self) -> ABCMemento:
        """
        Saves the current state inside a memento.
        """
        return Memento(self._state)

    def restore_from_memento(self, memento: ABCMemento) -> str:
        """
        Restores the Originator's state from a memento object.
        """
        self._state = memento.get_state()
        return self._state
