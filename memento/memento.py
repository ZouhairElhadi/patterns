from abc import ABC, abstractmethod


class ABCMemento(ABC):
    """
    The Memento interface provides a way to retrieve the memento's metadata,
    such as creation date or name. However, it doesn't expose the Originator's
    state.
    """

    @abstractmethod
    def set_state(self, state) -> None:
        pass

    @abstractmethod
    def get_state(self) -> str:
        pass


class Memento(ABCMemento):
    def __init__(self, state: str) -> None:
        self._state = state

    def set_state(self, state) -> None:
        """
        The Originator uses this method when to store a state in memento.
        """
        self._state = state

    def get_state(self) -> str:
        """
        The Originator uses this method when restoring its state.
        """
        return self._state
